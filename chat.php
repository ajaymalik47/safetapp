<?php
namespace MyApp;
use Guzzle\Common\Exception\RuntimeException;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use DB_Connect;
use Ratchet\Wamp\Exception;
use Home;
include_once 'includes/config.php';
require_once 'includes/DB_Connect.php';
include_once 'home/home.php';

$db = new DB_Connect();
$db->connect();

class Chat implements MessageComponentInterface  {
    protected $clients;
    const user_role = 'user';
    const driver_role = "driver";
    public function __construct() {
        $this->clients = new \SplObjectStorage;

    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {

        $msg = json_decode($msg,true);

            if(is_array($msg) && array_key_exists('user_id',$msg) == false  )
            {
                $this->sendMessage($from,'{"response":"error","message":"UserId not found"}');
                $from->close();
            }
        else {

            $from->uid = $msg['user_id'];
            $from->action = $msg['action'];
            $from->role = $msg['role'];
            $from->location = $msg['location'];
            $from->vid = $msg['vid'];

            $uid = $msg['user_id'];
            $checkUser = $this->checkUserId($uid);

            if($checkUser == false)
            {
                $this->sendMessage($from,'{"response":"error","message":"User not found"}');
                $from->close();
            }
            else {

                    $action = $msg['action'];

                    switch ($action) {
                        case "home": {

                            if($msg['role'] == self::user_role)
                            {

                                $drivers = $this->getAllClientByVehicleId($msg['vid'],self::driver_role);
                                $vehicle_type = Home::getHomeData($msg,$drivers);
                                $from->send(json_encode($vehicle_type));

                            }
                            elseif($msg['role'] == self::driver_role)
                            {
                                $this->sendDriverLocation($msg);
                            }
                            break;
                        }
                        case "driver_current_location": {

                            $this->sendDriverLocation($msg);
                            break;
                        }

                    }

                }
            }

    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages

        if($conn->role == 'driver')
        {
            self::removeDriver($conn);
        }

        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    public function removeDriver($driver)
    {
        $users = self::getAllUsers();
        foreach ($users as $user)
        {
            $msg['user_id'] = $driver->uid;
            $msg['action'] = 'remove_driver';
            $user->send(json_encode($msg));
        }
    }

    // Send driver location to all online users

    public function sendDriverLocation($msg)
    {
        $users = $this->getAllClientByVehicleId($msg['vid'],self::user_role);
        foreach ($users as $user)
        {
            $msg['u_location'] = $user->location;
            $user->send(json_encode($msg));
        }
    }

    public function getAllDrivers()
    {
        $drivers = array();
        foreach ($this->clients as $client) {

            if($client->role == 'driver')
            {
                $drivers[] = $client;
            }
        }
        return $drivers;
    }

    public function getAllUsers()
    {
        $drivers = array();
        foreach ($this->clients as $client) {

            if($client->role == 'user')
            {
                $drivers[] = $client;
            }
        }
        return $drivers;
    }

    public function getAllClientByVehicleId($vid,$type)
    {
        $vclients = array();
        foreach ($this->clients as $client ) {

            if(isset($client->vid) && $client->vid == $vid && $client->role == $type)
            {
                $vclients[] = $client;
            }
        }
        return $vclients;
    }

    public function getClient($uid)
    {
        foreach ($this->clients as $client) {
            if ($client->uid == $uid) {
                return $client;
            }
            else
            {  
                return false;
            }
        }
    }

//    public function setClientData($from,$msg)
//    {
//        foreach ($this->clients as $client) {
//            if ($client->resourceId == $from->resourceId) {
//
//                $client->uid = $msg['user_id'];
//                $client->action = $msg['action'];
//                $client->role = $msg['role'];
//                $client->location = $msg['location'];
//                $client->vid = $msg['vid'];
//                return $client;
//            }
//
//        }
//        return false;
//    }

    public function sendMessage($from,$msg)
    {
        try
        {
            $from->send($msg);
        }
        catch (Exception $e)
        {
            $e->getMessage();
        }
    }

    public function checkUserId($uid)
    {
        $uid = mysql_real_escape_string($uid);

        $db = mysql_query("select * from user_masters where user_id = '$uid'");
       // var_dump($db);
        if(is_resource($db))
        {
            $data = mysql_fetch_assoc($db);
            return $data;
        }
       else
       {
           return false;
       }
    }

}
