<?php
/**********************************************
 * File  : Available Ride                     *
 * Developer   : Anjali Rai                   *
 * Created Date: 08-April-2016                *
 *********************************************/

error_reporting(0);
require_once 'include/Map_Functions.php';
require_once 'include/DB_Connect.php';
# connecting to database
$db = new DB_Connect();
$db->connect();

header('Cache-Control: no-cache, must-revalidate');
header("Content-Type: application/json");


if(isset($_POST['pickupLat']) && isset($_POST['pickupLng']) && $_POST['pickupLat']!='' && $_POST['pickupLng']!=''){
    
        $pickupLat = $_POST['pickupLat'];
        $pickupLng = $_POST['pickupLng'];
        $dropLat = $_POST['dropLat'];
        $dropLng = $_POST['dropLng'];
        $vechileType = $_POST['vechileType'];
        $validate = 0;
        
        if(empty($vechileType) || $vechileType==''){
            $response['responseCode']="0";
            $response['responseMessage']="Please select vechile type.";
            $validate = 1;
        }
        
        if((empty($dropLat) || $dropLat=='' || empty($dropLng) || $dropLng=='') && $validate != 1){
            $response['responseCode']="0";
            $response['responseMessage']="Please select drop location.";
            $validate = 1;
        }       
         

       #Search the rows in the markers table
        $query = ("SELECT vehicle_type, base_fare, waiting_charge
                            FROM vehicle_category                           
                            WHERE id= ".$vechileType." 
                  ");
               
        $result = mysql_query($query);
        
        if (!$result && $validate == 0) {
            $response['responseCode']="0";
            $response['responseMessage']="Invalid query: " . mysql_error();
            $validate = 1;
        }
        
        $vechileDetails = mysql_fetch_assoc($result);
         //echo'<pre>'; print_r($vechileDetails); die;
        if($validate == 0){
            if(!empty($vechileDetails)){ 
                 $response['responseCode'] = '200';
                 $response['responseMessage'] ='Ride Estimate';

                $vDetails = array();
                $vDetails['type']= $vechileDetails['vehicle_type'];
                $vDetails['base_fare']= $vechileDetails['base_fare'];
                $vDetails['waiting_charge']= $vechileDetails['waiting_charge'];
                $response['vehicleDetails'] = $map->getRideEstimate($pickupLat, $pickupLng, $dropLat, $dropLng, $vDetails);
            }else{
                $response['responseCode']="200";
                $response['responseMessage']="No Cabs";
            }
        }
            #Return response
            $response1=json_encode($response);
            echo $response1;   
    
    }else{
        echo json_encode(array('responseCode'=>'0','responseMessage'=>'Please select the pickup location.'));          
    }



       
    
?>
