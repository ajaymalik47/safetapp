<?php
/************************************************************************************************
 * Android Web Services                                                                         *
 * Service: User Password                                                                       *
 * Created on: 11-04-2016                                                                       *
 * Developer: Anjali Rai                                                                        *
 * Description: Reset password and generates new password which is sent to the user via email.  *
 * Parameters:	EmailID only.                                                                   *
 * Test URL: http://alivenetsolution.co/safetapp/forgotpassword.php                             *   
 ***********************************************************************************************/

error_reporting(0);

require_once 'includes/DB_Connect.php';
# connecting to database
$db = new DB_Connect();
$db->connect();

header('Cache-Control: no-cache, must-revalidate');
header("Content-Type: application/json");

if(isset($_POST['email']) && $_POST['email']!=''){
    
    $validateCheck = 1;
    $email= trim($_POST['email']);   

    #Check validation
    if(empty($email) || $email == ''){
        $validateCheck = 0;        
        $errorMsg="Please enter your Email.";
    }
    
    if($validateCheck == 1){        
        $query = mysql_query("SELECT uc.user_id, uc.email, um.user_name FROM user_masters um join user_contacts uc on um.user_id=uc.user_id WHERE `email` = '$email'");
        $emailCheck =  mysql_fetch_assoc($query);
         
        if(empty($emailCheck)){
            $errorMsg = 'Email id does not exist.';           
            $validateCheck = 0;
       }
    }
    
    if($validateCheck == 1 && !empty($emailCheck)){
        $userId = $emailCheck['user_id'];
        $userName = $emailCheck['user_name'];
        $userEmail = $emailCheck['email'];
        
        $randomNo  =  'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $string1  =  str_shuffle($randomNo);
        $autoPassword  =  $string1{0} . $string1{1} . $string1{2} . $string1{3} . $string1{4} . $string1{5} . $string1{6} . $string1{7};
        $mdpasswordCode  =  md5(passkey.$autoPassword);
        $updatedPwd = mysql_query("UPDATE `user_contacts` SET `password` = '$mdpasswordCode', `updated` = Now() WHERE `user_contacts`.`user_id` = '$userId'");
        
        if($updatedPwd==1) {
            
            $to=$userEmail;                                                
            $subject = "Reset Password | SafeT App";
            $body='<table border="0" cellpadding="0" cellspacing="0" style="font-family:arial; margin:0 auto; width:700px">
            <tbody>
            <tr>
            <td style="background-color:#4D940E">
            <div style="padding:10px 20px;"></div>
            </td>
            </tr>
            <tr>
            <td style="background-color:#2B5604">
            <div style="padding:5px 10px;color:white;font-size: 18px;">Reset Password</div>
            </td>
            </tr>
            <tr>
            <td>
            <div style="border:1px solid green">
            <table style="color:#555; padding:20px">
            <tbody>
                    <tr>
                            <td>Dear'.$userName.'</td>
                    </tr>
                    <tr>
                            <td>&nbsp; </td>
                    </tr>
                    <tr>
                            <td>You have successfully reset your password.</td>
                    </tr>
                    <tr>
                            <td>Your Email Id is</td>
                    </tr>
                    <tr>
                            <td>'.$userEmail.'</td>
                    </tr>

                    <tr>
                            <td>Your Password is</td>
                    </tr>
                    <tr>
                            <td>'.$autoPassword.'</td>
                    </tr>

                    <tr>
                            <td>&nbsp;</td>
                    </tr>
                    <tr>
                            <td>&nbsp;</td>
                    </tr>
                    <tr>
                            <td>Thanks,</td>
                    </tr>
                    <tr>
                            <td>safeT Team</td>
                    </tr>
            </tbody>
            </table>
            </div>
            </td>
            </tr>
            <tr>
            <td style="background-color:#4D940E; text-align:right">
            <div style="padding:10px 20px;"></div>
            </td>
            </tr>
            </tbody>
            </table>';

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= "From: safeT<info@safeT.com>";
                $mail = mail($to,$subject,$body,$headers);
                
            $response['responseCode']="200";
            $response['responseMessage']="Your password has been updated successfully";
        }
        else{
                $validateCheck = 0;
                $errorMsg="Password updation failed";
        }
    }
    
    if($validateCheck == 0){
        $response['responseCode']="0";
        $response['responseMessage']= $errorMsg;
    }
            
    #Return response
    $response1=json_encode($response);
    echo $response1;           
               
 }else{
        echo json_encode(array('responseCode'=>'0','responseMessage'=>'Email ID is mandatory!'));          
    }



?>
