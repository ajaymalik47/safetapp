<?php
/**********************************************
 * File  : User Logout                        *
 * Developer   : Anjali Rai                   *
 * Created Date: 08-April-2016                *
 *********************************************/

//error_reporting(0); 

require_once 'includes/DB_Connect.php';
# connecting to database
$db = new DB_Connect();
$db->connect();

header('Cache-Control: no-cache, must-revalidate');
header("Content-Type: application/json");

if(isset($_POST['session']) && $_POST['session']=='expire'){
    if(isset($_POST['user_id'])){
        $userId = mysql_real_escape_string($_POST['user_id']);
        $updatedAccessKey = mysql_query("UPDATE `user_masters` SET `access_key` = '', `updated` = Now() WHERE `user_masters`.`user_id` = '$userId'");
    
        $response['responseCode']="200";
        $response['responseMessage']="Successfully logout.";
    }
    else{
        $response['responseCode']="0";
        $response['responseMessage']="User id missing.";
    }
    
    #Return response
    $response1=json_encode($response);
    echo $response1;
    
}else{
    echo json_encode(array('responseCode'=>'0','responseMessage'=>'Please set session.'));          
}



?>
