<?php
/**********************************************
 * File  :  Ride Cancellation                 *
 * Developer   : Anjali Rai                   *
 * Created Date: 20-April-2016                *
 *********************************************/

error_reporting(0);
include('twilio/smsapi.php');
require_once 'include/DB_Connect.php';
# connecting to database
$db = new DB_Connect();
$db->connect();

$s=new Smsapi();

header('Cache-Control: no-cache, must-revalidate');
header("Content-Type: application/json");


if(isset($_POST['userId']) && $_POST['userId']!=''){
    
        $userId = $_POST['userId'];
        $tokenId = $_POST['tokenId'];
        $reason = $_POST['reason'];
        $status = $_POST['status'];       
        
        #Check Validation        
        if(empty($tokenId) || $tokenId == ''){
            $validateError = 1;
            $error="Token Id is blank.";
        }
        if((empty($reason)|| $reason == '') && $validateError ==0){
            $validateError = 1;
            $error="Please provide reason for cancellation.";
        }
        if((empty($status)|| $status == '') && $validateError ==0){
            $validateError = 1;
            $error="Please provide cancellation status.";
        }
        if($status != 1 && $validateError ==0){
            $validateError = 1;
            $error="Cancellation status not true.";
        }
        
        #Cancel Ride
        if($validateError ==0){ 
             $insertQuery = "INSERT INTO cancellation_request_status(transaction_ref_no, cancellation_reason, status, created)VALUES('$tokenId', '$reason', '$status', Now())";
            $insertData=mysql_query($insertQuery);   
            if($insertData){
                $query = "SELECT uc.mobile_no, um.user_name FROM user_masters um 
                            INNER JOIN user_contacts uc on um.user_id=uc.user_id 
                            WHERE um.user_id= ".$userId." ";
                $result = mysql_query($query);                
                $userData=mysql_fetch_assoc($result); 
                $cancelMeassage = 'Hi '.$userData['user_name'].' your booking '.$tokenId.' has been cancelled as per your request. Looking forward to serving you again.';
                $sms = $s->sendsms_api('918750148020',$userDetails['name'],$cancelMeassage);
            }
        }
        
        #Return Response
         if($validateError == 1){
              $response['responseCode']="0";
              $response['responseMessage']=$error;
          }else{
                $response['responseCode']="200";
                $response['responseMessage']="BOOKING_CANCEL";
                $response['text']="Your booking with token_id $tokenId has been cancelled successfully.";
          } 
        
          $response1=json_encode($response);
            echo $response1;
    }else{
        echo json_encode(array('responseCode'=>'0','responseMessage'=>'User id not found.'));          
    }



       
    
?>
