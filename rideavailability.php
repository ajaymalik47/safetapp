<?php
/**********************************************
 * File  : Available Ride                     *
 * Developer   : Anjali Rai                   *
 * Created Date: 08-April-2016                *
 *********************************************/

error_reporting(0);
require_once 'include/Map_Functions.php';
require_once 'include/DB_Connect.php';
# connecting to database
$db = new DB_Connect();
$db->connect();

header('Cache-Control: no-cache, must-revalidate');
header("Content-Type: application/json");


if(isset($_POST['pickupLat']) && isset($_POST['pickupLng']) && $_POST['pickupLat']!='' && $_POST['pickupLng']!=''){
    
        $lat = $_POST['pickupLat'];
        $lon = $_POST['pickupLng'];
        $vechileType = $_POST['vechileType'];
        $radius = '12'; // Km

        
        if(empty($vechileType) || $vechileType==''){
            $response['responseCode']="0";
            $response['responseMessage']="Please select vechile type.";
        }

        // Search the rows in the markers table
        $query = sprintf("SELECT temp.id, temp.driver_latitude, temp.driver_longitude, v.vehicle_name, v.vehicle_category_id, vc.vehicle_type,vc.base_fare, vc.waiting_charge,(6371 * acos( cos( radians('%s') ) * cos( radians(driver_latitude) ) * cos( radians(driver_longitude) - radians('%s') ) + sin( radians('%s') ) * sin( radians(driver_latitude) ) ) ) AS distance 
                            FROM temp_drivers_locations temp 
                            INNER JOIN vehicle v on temp.driver_id=v.user_id  
                            INNER JOIN vehicle_category vc on vc.id=v.vehicle_category_id 
                            WHERE v.vehicle_category_id= ".$vechileType." 
                            HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
          mysql_real_escape_string($lat),
          mysql_real_escape_string($lon),
          mysql_real_escape_string($lat),
          mysql_real_escape_string($radius));
               
        $result = mysql_query($query);
        
        if (!$result) {
            $response['responseCode']="0";
            $response['responseMessage']="Invalid query: " . mysql_error();
        }
        $rideDetails = mysql_fetch_assoc($result);
         
        if(!empty($rideDetails)){ 
             $response['responseCode'] = '200';
             $response['responseMessage'] ='Nearest Cab found';
             $driverLat = $rideDetails['driver_latitude'] ;
            $driverLon = $rideDetails['driver_longitude'] ;
            $vDetails = array();
            $vDetails['type']= $rideDetails['vehicle_type'];
            $vDetails['vehicle_name']= $rideDetails['vehicle_name'];
            $vDetails['base_fare']= $rideDetails['base_fare'];
            $vDetails['waiting_charge']= $rideDetails['waiting_charge'];
            $response['vehicleDetails'] = $map->getDrivingDistance($driverLat, $driverLon, $lat, $lon, $vDetails);
        }else{
            $response['responseCode']="200";
            $response['responseMessage']="No Cabs";
        }
            #Return response
            $response1=json_encode($response);
            echo $response1;   
    
    }else{
        echo json_encode(array('responseCode'=>'0','responseMessage'=>'Please select the pickup location.'));          
    }



       
    
?>
