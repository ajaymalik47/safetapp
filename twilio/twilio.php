<?php

require_once 'System/Twilio.php';

class Twilio
{
    public $client;
    public   $TWILIO_ACCOUNT_SID = 'AC63406acac5aa7bd54e231ab631bc26d6';
    public $token = "5430f5320bae6f6099b2ab550eff13fc";
    public $TWILIO_CONFIGURATION_SID = 'VSa3b967d7d5612608807628b2b786c3b5';

    public function __construct()
    {
        $this->client = new Services_Twilio($this->TWILIO_ACCOUNT_SID, $this->token);
    }


    public function createUser($user_name)
    {
        $user_info = array();

        $key = $this->client->account->keys->create(array("FriendlyName" => trim($user_name)));

        $user_info['user_sid']=$key->sid;
        $user_info['user_secret_key'] = $key->secret;
        $user_info['user_name'] = $key->friendly_name;

        return $user_info;
    }

    public function checkUser($user_name)
    {
        foreach ($this->client->account->keys as $account) {

              if($account->friendly_name == trim($user_name))
              {
                  return 'exist';
              }
        }
    }

    public function getAccessToken($TWILIO_API_KEY,$TWILIO_API_SECRET,$identity)
    {
        $token = new Services_Twilio_AccessToken(
            $this->TWILIO_ACCOUNT_SID,
            $TWILIO_API_KEY,
            $TWILIO_API_SECRET,
            3600,
            $identity
        );

// Grant access to Conversations
        $grant = new Services_Twilio_Auth_ConversationsGrant();
        $grant->setConfigurationProfileSid($this->TWILIO_CONFIGURATION_SID);
        $token->addGrant($grant);

        return json_encode(array(
            'identity' => $identity,
            'token' => $token->toJWT(),
        ));
    }


    // set Admin keys

    public function getAdminAccessToken()
    {
        $token = new Services_Twilio_AccessToken(
            $this->TWILIO_ACCOUNT_SID,
            'SK122c96d2b7a74a122ea0a26af3540ac6',
            'wvX2NnOJAi2XlO4g4Oq6s5ocnSIqqTkT',
            3600,
            'admin'
        );

// Grant access to Conversations
        $grant = new Services_Twilio_Auth_ConversationsGrant();
        $grant->setConfigurationProfileSid($this->TWILIO_CONFIGURATION_SID);
        $token->addGrant($grant);

        return json_encode(array(
            'identity' => 'admin',
            'token' => $token->toJWT(),
        ));
    }

}