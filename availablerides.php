<?php
/**********************************************
 * File  : Available Ride                     *
 * Developer   : Anjali Rai                   *
 * Created Date: 08-April-2016                *
 *********************************************/

//error_reporting(0);
require_once 'include/Map_Functions.php';
require_once 'include/DB_Connect.php';
# connecting to database
$db = new DB_Connect();
$db->connect();

header('Cache-Control: no-cache, must-revalidate');
header("Content-Type: application/json");


if(isset($_POST['pickupLat']) && isset($_POST['pickupLng']) && $_POST['pickupLat']!='' && $_POST['pickupLng']!=''){
    
        $lat = $_POST['pickupLat'];
        $lon = $_POST['pickupLng'];
        $radius = '12'; // Km

        // Search the rows in the markers table
        $query = sprintf("SELECT temp.id, temp.driver_latitude, temp.driver_longitude, v.vehicle_name, v.vehicle_category_id,vc.base_fare,vc.waiting_charge,(6371 * acos( cos( radians('%s') ) * cos( radians(driver_latitude) ) * cos( radians(driver_longitude) - radians('%s') ) + sin( radians('%s') ) * sin( radians(driver_latitude) ) ) ) AS distance 
                            FROM temp_drivers_locations temp 
                            INNER JOIN vehicle v on temp.driver_id=v.user_id 
                            INNER JOIN vehicle_category vc on vc.id=v.vehicle_category_id 
                            HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
          mysql_real_escape_string($lat),
          mysql_real_escape_string($lon),
          mysql_real_escape_string($lat),
          mysql_real_escape_string($radius));
               
        $result = mysql_query($query);
        
        if (!$result) {
            $response['responseCode']="0";
            $response['responseMessage']="Invalid query: " . mysql_error();
        }
        
         #Iterate through the rows
            while($row = mysql_fetch_assoc($result)){
                   $rideDetails[] = $row;
            }
            
            $miniDetails = array_filter($rideDetails, function($v) { return $v['vehicle_category_id'] == '1'; });
            $sedanDetails = array_filter($rideDetails, function($v) { return $v['vehicle_category_id'] == '2'; });
            $primeDetails = array_filter($rideDetails, function($v) { return $v['vehicle_category_id'] == '3'; });
                    
            $response = array();
            if(!empty($rideDetails)){ 
                $response['responseCode'] = '200';
               $response['responseMessage'] ='Nearest Cab found';
                if(!empty($miniDetails) || $miniDetails != 'null'){
                    $miniDetail = array_shift($miniDetails);
                    $driverLat = $miniDetail['driver_latitude'] ;
                    $driverLon = $miniDetail['driver_longitude'] ;
                    $vDetails = array();
                    $vDetails['type']= "Mini";
                    $vDetails['vehicle_name']= $miniDetail['vehicle_name'];
                    $vDetails['base_fare']= $miniDetail['base_fare'];
                    $vDetails['waiting_charge']= $miniDetail['waiting_charge'];              
                    $response['categories']['Mini'] = $map->getDrivingDistance($driverLat, $driverLon, $lat, $lon, $vDetails);
                }
                if(!empty($sedanDetails) || $sedanDetails != 'null'){
                    $sedanDetail = array_shift($sedanDetails);
                    $driverLat = $sedanDetail['driver_latitude'] ;
                    $driverLon = $sedanDetail['driver_longitude'] ;
                    $vDetails = array();
                    $vDetails['type']= "Sedan";
                    $vDetails['vehicle_name']= $sedanDetail['vehicle_name'];
                    $vDetails['base_fare']= $sedanDetail['base_fare'];
                    $vDetails['waiting_charge']= $sedanDetail['waiting_charge'];
                    $response['categories']['Sedan'] = $map->getDrivingDistance($driverLat, $driverLon, $lat, $lon, $vDetails);
                    }
                if(!empty($primeDetails) || $primeDetails != '' || $primeDetails != "Null"){
                    $primeDetail = array_shift($primeDetails);
                    $driverLat = $primeDetail['driver_latitude'] ;
                    $driverLon = $primeDetail['driver_longitude'] ;
                    $vDetails = array();
                    $vDetails['type']= "Prime";
                    $vDetails['vehicle_name']= $primeDetail['vehicle_name'];
                    $vDetails['base_fare']= $primeDetail['base_fare'];
                    $vDetails['waiting_charge']= $primeDetail['waiting_charge'];
                    $response['categories']['Prime'] = $map->getDrivingDistance($driverLat, $driverLon, $lat, $lon, $vDetails);
                    }
         }else{
            $response['responseCode']="200";
            $response['responseMessage']="No Cabs";
        }
            #Return response
            $response1=json_encode($response);
            echo $response1;
    
    }else{
        echo json_encode(array('responseCode'=>'0','responseMessage'=>'Please select the pickup location.'));          
    }



       
    
?>
