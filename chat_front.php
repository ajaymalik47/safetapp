<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/html">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Socket Test 2</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script>

    var map;
    function initMap() {

        var latlng = new google.maps.LatLng($('#lat').val(), $('#long').val());
         map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Set lat/lon values for this property',
            draggable: true
        });
        google.maps.event.addListener(marker, 'dragend', function(a) {
            $('#lat').val(a.latLng.lat().toFixed(4));
            $('#long').val(a.latLng.lng().toFixed(4));

            role = $('#role').val();

            if(role == 'driver')
            {
                driver_current_location();
            }
            else
            {
                sendData();
            }
        });
    }

    var origins = [];
    var markers = {};
    var destinations = [];

    var query = {
        origins: origins,
        destinations: destinations,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC
    };

    function getDistance() {

        dms = new google.maps.DistanceMatrixService();
        dms.getDistanceMatrix(query, function (response, status) {
                if (status == "OK") {
                   // console.log(response.rows);
                    populateTable(response.rows);
                }
            }
        );
    }

    function addMarker(lat,long,did) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, long),
            map: map
        });

        markers[did]=marker;
        console.log(markers);
    }

    function updateMarker(did,lat,long) {
        marker=markers[did];
        marker.setPosition( new google.maps.LatLng(lat, long));
    }

    function populateTable(rows) {
       // console.log(rows);
        for (var i = 0; i < rows.length; i++) {
            for (var j = 0; j < rows[i].elements.length; j++) {
                var distance = rows[i].elements[j].distance.text;
                var duration = rows[i].elements[j].duration.text;

                $('#response').append("<br>"+'Distance : '+distance + ' Duration : '+duration+"<br>");

//                    var td = document.getElementById('element-' + i + '-' + j);
//                    td.innerHTML = distance + "<br/>" + duration;
            }
        }
    }



    var conn = new WebSocket('ws://172.17.56.53:8080');
    conn.onopen = function(e) {
        console.log("Connection established!");
        // conn.send('{"user_id":"abc87dfg","login":"check"}');
    };

    conn.onmessage = function(e) {
        console.log(e.data);
        data = e.data;
        data = JSON.parse(data);
        if(data.action == 'home') {

            if(data.role == 'user') {
                query.destinations = [data.u_location.lat + ',' + data.u_location.long];
                $.each(data['vehicle_details'], function (e, ij) {
                    console.log(ij);
                    $.each(ij, function (e, i) {
                        query.origins = [i['d_lat'] + ',' + i['d_long']];
                        if (markers[i['did']]) {
                            updateMarker(i['did'], i['d_lat'], i['d_long']);
                            getDistance();
                        }
                        else {
                            addMarker(i['d_lat'], i['d_long'], i['did']);
                            getDistance();
                        }
                        //  addMarker(i['d_lat'], i['d_long'],data.user_id);
                        //   getDistance();
                    });
                })
            }
            else if(data.role == 'driver')
            {
                query.destinations = [data.u_location.lat + ',' + data.u_location.long];
                query.origins = [data.location['lat'] + ',' + data.location['long']];
                if(markers[data.user_id]) {
                    updateMarker(data.user_id,data.location['lat'], data.location['long']);
                    getDistance();
                }
                else
                {
                    addMarker(data.location['lat'], data.location['long'],data.user_id);
                    getDistance();
                }
            }
        }

        else if(data.action == "driver_current_location")
        {
            query.destinations = [data.u_location.lat + ',' + data.u_location.long];
            query.origins = [data.location['lat'] + ',' + data.location['long']];
            if(markers[data.user_id]) {
                updateMarker(data.user_id,data.location['lat'], data.location['long']);
                getDistance();
            }
            else
            {
                addMarker(data.location['lat'], data.location['long'],data.user_id);
                getDistance();
            }
        }
        else if(data.action == 'remove_driver')
        {
            if(markers[data.user_id])
            {
                marker = markers[data.user_id];
                marker.setMap(null);
                markers[data.user_id] = false;
//                marker.addListener("dblclick", function() {
//
//                });

            }
        }

       // $('#response').html("<p> "+ e.data +" </p>");
    };

    conn.close = function(e) {
        console.log(e.data);
    };


    function driver_current_location() {
        action = $('#action').val();
        uid = $('#uid').val();
        role = $('#role').val();
        lat = $('#lat').val();
        long = $('#long').val();
        vid = $('#vid').val();

        data = '{"user_id":"'+uid+'","action":"driver_current_location","role":"'+role+'","location":{"lat":"'+lat+'","long":"'+long+'","name":"Delhi"},"vid":"'+vid+'"}';
        conn.send(data);

    }

    function sendData() {
        action = $('#action').val();
        uid = $('#uid').val();
        role = $('#role').val();
        lat = $('#lat').val();
        long = $('#long').val();
        vid = $('#vid').val();

        data = '{"user_id":"'+uid+'","action":"'+action+'","role":"'+role+'","location":{"lat":"'+lat+'","long":"'+long+'","name":"Delhi"},"vid":"'+vid+'"}';
        conn.send(data);

    }

</script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLHYRYaMncm8PtqcIIj9EeNk-c3p23dOc&callback=initMap" async defer></script>
</head>
<body>
        <input type="text" placeholder="action" value="home" id="action"/><br>
        <input type="text" placeholder="user id" value="15" id="uid"/><br>
       <select id="role">
        <option>user</option>
        <option>driver</option>
        </select>
        <br>
        <input type="text" placeholder="lat" value="28.5494489" id="lat"/><br>
        <input type="text" placeholder="long" value="77.2001368" id="long"/><br>
        <input type="text" placeholder="vid" value="1" id="vid"/><br>
        <input type="button" id="send" onclick="sendData()" value="send"/>
<div id="response" style="width: 400px"></div>

<div id="map" style="height: 400px; width: 400px; margin-left: 50%;margin-top: -11%"></div>
<div id="vehicle_type"></div>
</body>
</html>
