<?php
/**********************************************
 * File  :  Ride Booking                      *
 * Developer   : Anjali Rai                   *
 * Created Date: 18-April-2016                *
 *********************************************/

error_reporting(0);

define('TIMEZONE', 'Asia/Calcutta'); // INDIA
date_default_timezone_set(TIMEZONE);

require_once 'include/Map_Functions.php';
require_once 'include/DB_Connect.php';
# connecting to database
$db = new DB_Connect();
$db->connect();

header('Cache-Control: no-cache, must-revalidate');
header("Content-Type: application/json");

$validateError = 0;
$error = '';
$driverResponse ='';
$tokenId = '';
if(isset($_POST['userId']) && $_POST['userId']!='' && $_POST['pickupMode'] == 'Now'){
    
        $userId = $_POST['userId'];
        $pickupAddress = $_POST['pickupAddress'];
        $pickupLat = $_POST['pickupLat'];
        $pickupLon = $_POST['pickupLng'];
        $destinationAddress = $_POST['destinationAddress'];
        $requestTime = $_POST['requestTime'];
        $driverResponse = $_POST['driverResponse'];
        $tokenId = $_POST['tokenId'];
       // $pickupMode = $_POST['pickupMode'];
        $vechileType = $_POST['vechileType'];
        
        
        
        #Check Validation
        if($driverResponse == '' && $tokenId == ''){
            if(empty($pickupLat) || empty($pickupLon)){
                $validateError = 1;
                $error="Please select the pickup location.";
            }

            if((empty($vechileType)|| $vechileType == '') && $validateError ==0){
                $validateError = 1;
                $error="Please select Vechile Type.";
            }
        }
        if($validateError ==0){ 
        #Get nearest drivers
            if($driverResponse == ''){
                $radius = '25'; // Km
                $query = sprintf("SELECT temp.driver_id,temp.driver_address,temp.driver_latitude, temp.driver_longitude, (6371 * acos( cos( radians('%s') ) * cos( radians(driver_latitude) ) * cos( radians(driver_longitude) - radians('%s') ) + sin( radians('%s') ) * sin( radians(driver_latitude) ) ) ) AS distance 
                                    FROM temp_drivers_locations temp 
                                    INNER JOIN vehicle v on temp.driver_id=v.user_id                              
                                    WHERE v.vehicle_category_id= ".$vechileType." 
                                    HAVING distance < '%s' ORDER BY distance LIMIT 0 , 20",
                  mysql_real_escape_string($pickupLat),
                  mysql_real_escape_string($pickupLon),
                  mysql_real_escape_string($pickupLat),
                  mysql_real_escape_string($radius)); 
               $result = mysql_query($query); 
               $driverDetail= array(); 
               $i =0;
               while($getData=mysql_fetch_assoc($result)){
                   $driverDetail[$i]['driver_id'] = $getData['driver_id'];
                   $driverDetail[$i]['driver_latitude'] = $getData['driver_latitude'];
                   $driverDetail[$i]['driver_longitude'] = $getData['driver_longitude'];
                   //$driverDetail[$i]['driver_address'] = $getData['driver_address'];
                   $i++;
               }              
            }
            
            
        #Ride Now functionality
        if($driverResponse != '' && $tokenId != ''){ 
            if($driverResponse == 0){
                
                $driverDetail = arrya();
                $driverDetail =  $_POST['$driverDetail']; 
                $driverDetail = array_shift($driverDetail);
            }
            
            $rideUpdate = mysql_query("UPDATE `ride_now` SET `request_status` = '$driverResponse', `updated` = Now() WHERE `ride_now`.`transaction_no` = '$tokenId'");
            
            if($driverResponse == 1){
                $query = "SELECT uc.mobile_no, um.user_name, v.vehicle_name, v.vehicle_registration_no, rn.expected_time
                FROM user_masters um 
                INNER JOIN user_contacts uc on um.user_id=uc.user_id 
                INNER JOIN vehicle v on v.user_id=uc.user_id
                INNER JOIN ride_now rn on um.user_id=rn.request_to                   
                WHERE rn.transaction_no= '$tokenId'";
                
                $result = mysql_query($query); 
                $userData=mysql_fetch_assoc($result);                
                $driverArrivalTime = date("g:i A,d-M", strtotime($userData['expected_time']));
               
                #add message functionality
                include('twilio/smsapi.php');
                $s=new Smsapi();
                
                $meassageToUser = htmlentities('Hi your driver '.$userData['user_name'].' '.$userData['mobile_no'].' for '.$tokenId.' '.$userData['vehicle_name'].' '.$userData['vehicle_registration_no'].' to pick you up @ '.$driverArrivalTime.'.');
                $sms = $s->sendsms_api('918750148020',$userData['user_name'],$meassageToUser);                              
            }
        }
       
        elseif($driverDetail != '' && ($driverResponse == '' || $driverResponse == 0)){ 
            #random transaction Refrence No. 
            $randomNo  =  'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            $string1  =  str_shuffle($randomNo);
            $transactionRefNo  =  $string1{0} . $string1{1} . $string1{2} . $string1{3} . $string1{4} . $string1{5} . $string1{6} . $string1{7};
            
            $driverId = $driverDetail[0]['driver_id'];
            $driverLat = $driverDetail[0]['driver_latitude'];
            $driverLon = $driverDetail[0]['driver_longitude'];
            $driverCurrentDetail = $map->getTimeEstimate($driverLat, $driverLon, $pickupLat, $pickupLon);   
            $expectedTime = date("Y/m/d H:i:s a", strtotime("+".$driverCurrentDetail['time']." "));

            //$expectedTime = $driverCurrentDetail['time'];
            $driverAddress = $driverCurrentDetail['sourceAddress'];
            if($driverResponse == ''){
                $driverResponse = 2;
            }
            
            $insertQuery = "INSERT INTO ride_now(transaction_no, request_from, request_to, user_current_location, user_latitude, user_longitude, driver_current_location, driver_latitude, driver_longitude, destination, request_time, expected_time, request_status, created)
                                        VALUES('$transactionRefNo', '$userId', '$driverId', '$pickupAddress', '$pickupLat', '$pickupLon','$driverAddress','$driverLat','$driverLon','$destinationAddress','$requestTime','$expectedTime','$driverResponse', Now())";
            $insertData=mysql_query($insertQuery);    
            
        }else{
            $validateError = 1;
            $error="No Driver found.";
        }
    }
        #Return Response
         if($validateError == 1){
              $response['responseCode']="0";
              $response['responseMessage']=$error;
          }else{
                $response['responseCode']="200";
                $response['responseMessage']="User Details.";
                $response['tokenId']=$transactionRefNo;
                $response['userPickupLocation']=$pickupAddress;
                $response['userDropLocation']=$destinationAddress;
                $response['rideTime']=$requestTime;
                $response['driverDetail']=$driverDetail;
                $response['rideStatus']=$driverResponse;
                
          } 
          $response1=json_encode($response);
            echo $response1;
    }else{
        echo json_encode(array('responseCode'=>'0','responseMessage'=>'User id or pickup mode not found.'));          
    }



       
    
?>
