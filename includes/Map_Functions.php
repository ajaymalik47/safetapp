<?php

class Map_Functions {
    
     function getDrivingDistance($lat1, $long1, $lat2, $long2, $vDetails){
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=en-US";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);
            $dist = $response_a['rows']['0']['elements']['0']['distance']['text'];
            $distMtr = $response_a['rows']['0']['elements']['0']['distance']['value'];
            $time = $response_a['rows']['0']['elements']['0']['duration']['text'];
            $timeSec = $response_a['rows']['0']['elements']['0']['duration']['value'];
            $status = $response_a['rows']['0']['elements']['0']['status'];
            $address = trim($response_a['origin_addresses']['0'], '()');
            $destinationAddress = trim($response_a['destination_addresses']['0'], '()');
       
            ($status)?$responsecode='200':$responsecode='0';
            if($status == "NOT_FOUND"){
                return array('vechileType'=>$vDetails['type'],'status'=>$status,);
            }else{
                return array('vechileType'=>$vDetails['type'],
                             'vechileName'=>$vDetails['vehicle_name'],
                             'distance' => $dist,
                             'distanceMeter'=>$distMtr, 
                             'time' => $time, 
                             'timeSec'=>$timeSec,
                             'driverLocation'=>$address,
                             'destinationLocation'=>$destinationAddress,
                             'baseFare'=>$vDetails['base_fare'],
                             'waitingCharge'=>$vDetails['waiting_charge'],
                             'status'=>$status,
                           );
            }
            
        }
        
        function getRideEstimate($lat1, $long1, $lat2, $long2, $vDetails){
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=en-US";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);
            $dist = $response_a['rows']['0']['elements']['0']['distance']['text'];            
            $time = $response_a['rows']['0']['elements']['0']['duration']['text'];          
            $status = $response_a['rows']['0']['elements']['0']['status'];
            $sourceAddress = trim($response_a['origin_addresses']['0'], '()');
            $destinationAddress = trim($response_a['destination_addresses']['0'], '()');
       
            ($status)?$responsecode='200':$responsecode='0';
            if($status == "NOT_FOUND"){
                return array('vechileType'=>$vDetails['type'],'status'=>$status,);
            }else{
                return array('vechileType'=>$vDetails['type'],
                             'distance' => $dist,
                             'time' => $time, 
                             'sourceAddress'=>$sourceAddress,
                             'destinationAddress'=>$destinationAddress,
                             'baseFare'=>$vDetails['base_fare'],
                             'waitingCharge'=>$vDetails['waiting_charge'],
                             'amount_min'=> $vDetails['base_fare']*$dist,
                           );
            }
            
        }
        
         function getTimeEstimate($lat1, $long1, $lat2, $long2){
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=en-US";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response, true);                     
            $time = $response_a['rows']['0']['elements']['0']['duration']['text'];          
            $status = $response_a['rows']['0']['elements']['0']['status'];
            $sourceAddress = trim($response_a['origin_addresses']['0'], '()');
           
       
            ($status)?$responsecode='200':$responsecode='0';
            if($status == "NOT_FOUND"){
                return array('status'=>$status,);
            }else{
                return array(
                             'time' => $time, 
                             'sourceAddress'=>$sourceAddress,                             
                           );
            }
            
        }
    
}
$map=new Map_Functions();
?>
