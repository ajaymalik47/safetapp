<?php

class DB_Functions {

    # constructor
    function __construct() {
        require_once 'DB_Connect.php';
        # connecting to database
        $this->db = new DB_Connect();
        $this->db->connect();
    }

    
     #Registration Validations
    public function regValidation($details) {     
        foreach($details as $key => $value){
           if(empty($value)){          
                $response="Missing ".$key."";
                return $response;
            }
        }
        if (array_key_exists('gender',$details)){
            if(($details['gender']!= 'male' && $details['gender']!= "female")){
                $response="Gender value not found.";
                return $response;
            }
        }

    }
    
    

    # user insert  
     public function addUser($details) {  
         $role = '';
         if($details['role'] == 'driver'){
             $role = 1;
         }elseif($details['role'] == 'user'){
             if($details['gender'] == 'male'){
                 $role = 2;
             }elseif($details['gender'] == 'female'){
                 $role = 3;
             }
         }
         
        $name = $details['name'];
        $gender= $details['gender'];
        
        $password = md5(passkey.$details['password']); // encrypted password
        $email = $details['email'];
        $phone = $details['phone']; 
        $phoneVerficationCode = $details['verifyMobile']; 
        
        $address = $details['address'];
        $city = $details['city'];
        $state = $details['state'];
        $country = $details['country'];
        $pin_code = $details['pinCode'];  
        
        $dob = $details['dob'];
        $licenceNo = $details['licenceNo'];
        $licenceIssueDate = $details['licenceIssueDate'];
        $licenceValidity = $details['licenceValidity'];
        
        $currentTime = date('Y-m-d H:i:s');
       
        $query = "INSERT INTO user_masters(user_role, user_name, gender, status, created, updated)VALUES('$role', '$name', '$gender', '0', Now(), Now())";
	$result=mysql_query($query);
        $user_id = mysql_insert_id();
        
        if($result){
            $userContacts = mysql_query("INSERT INTO user_contacts(user_id, mobile_no, email, password, mobile_verification_code, created, updated)VALUES('$user_id', '$phone', '$email', '$password', '$phoneVerficationCode', Now(), Now())");
            $userAddress = mysql_query("INSERT INTO user_addresses(user_id, street_address, city, state, country, zip_code, created, updated)VALUES('$user_id', '$address', '$city', '$state', '$country', '$pin_code', Now(), Now())");
            
            if($details['role'] == 'driver'){
               $driverDetails = mysql_query("INSERT INTO driver_licence_details(user_id, dob, licence_no, licence_issue_date, licence_validity, created, updated)VALUES('$user_id', '$dob', '$licenceNo', '$licenceIssueDate', '$licenceValidity', Now(), Now())");
            }
            
            # return user details
            return $user_id;
        }else {
            return false;
        }
    }
  

    #Check email is existed or not
    public function isEmailExisted($email) {
       $email= mysql_real_escape_string($email);
        $result = mysql_query("SELECT email, is_mobile_verify from user_contacts WHERE email = '$email'");
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
            # email existed 
           // return true;
            return $result;
        } else {
            # email not existed
            return false;
        }
    }
    
    #Check Phone is existed or not
    public function isPhoneExisted($phone) {
        $result = mysql_query("SELECT mobile_no from user_contacts WHERE mobile_no = '$phone'");
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
            # Phone existed 
            return true;
        } else {
            # Phone not existed
            return false;
        }
    }


}
$db=new DB_Functions();
?>
