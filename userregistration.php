<?php
/**********************************************
 * File  : User Registration                  *
 * Developer   : Anjali Rai                   *
 * Created Date: 08-April-2016                *
 *********************************************/

//error_reporting(0);
include('twilio/smsapi.php');
require_once 'includes/DB_Functions.php'; 
$s=new Smsapi();
//$sms = $s->sendsms_api('919873112845','Anjali','1234');
//echo("hello"); die;

header('Cache-Control: no-cache, must-revalidate');
header("Content-Type: application/json");

if(isset($_POST['role']) && $_POST['role']!='' && ($_POST['role']=='user' || $_POST['role']=='driver')){
    
            $validateCheck  =  1;
            $userDetails = array();
            
            # Request type is Register new user
            $userDetails['role'] = $_POST['role'];
            $userDetails['name'] = ucwords(strtolower(trim($_POST['name'])));
            $userDetails['password'] = $_POST['password'];
            $userDetails['email'] = strtolower(trim($_POST['email']));
            $userDetails['address'] = ucwords(strtolower(trim($_POST['address'])));
            $userDetails['phone'] = trim($_POST['phone']);
            $userDetails['gender'] = $_POST['gender'];
            $userDetails['city'] = ucwords(strtolower(trim($_POST['city'])));
            $userDetails['state'] = ucwords(strtolower(trim($_POST['state']))); 
            $userDetails['country'] = ucwords(strtolower(trim($_POST['country']))); 
            $userDetails['pinCode'] = trim($_POST['pinCode']);
            
            if($_POST['role']=='driver'){
                $userDetails['licenceNo'] = trim($_POST['licenceNo']);
                $userDetails['dob'] = $_POST['dob'];           
                $userDetails['licenceIssueDate'] = $_POST['licenceIssueDate'];
                $userDetails['licenceValidity'] = $_POST['licenceValidity'];
            }
            
            $email = $userDetails['email'];
            $password = $_POST['password'];
            $phoneNumber = '+918750148020';
            
            #Check the validation
            $error = '';
            $error = $db->regValidation($userDetails);
            if(!empty($error)){
                $response['responseCode']="0";
                $response['responseMessage']="$error";
            }
            
            # check if Email is already existed
            elseif ($db->isEmailExisted($email)) {
                    # Email is already existed - error response
                    $response['responseCode']="0";
                    $response["responseMessage"] = "Email already existed";
            }
            
            # check if Mobile No. is already existed
            elseif ($db->isPhoneExisted($userDetails['phone'])) {
                    # Mobile No. is already existed - error response
                    $response['responseCode']="0";
                    $response["responseMessage"] = "Phone already existed";
            }
            
            # Insert user
            else{
                    #random password set for user account
                    $randomNo  =  '0123456789';
                    $string1  =  str_shuffle($randomNo);                   
                    $autoPassword  =  $string1{0} . $string1{1} . $string1{2} . $string1{3};                   
                    $activationCode  =  md5($autoPassword);
                    
                    # Insert user
                    $userDetails['verifyMobile'] = $activationCode;
                    //$userDetails['verifyMobile'] = md5(1234);
                    $userId = $db->addUser($userDetails);
                
                if(!empty($userId)){ 
                    # Send Sms
                    $msg = "SafeT mobile verification Code: ".$autoPassword."";
                   // $sms = $s->sendsms_api($phoneNumber,$userDetails['name'],$autoPassword);
                    $sms = $s->sendsms_api('918750148020',$userDetails['name'],$msg);
                    $response['responseCode']="200";
                    $response['responseMessage']="You have been registered successfully";
                    $response['userId']=$userId;
                    $response['userName']=$userDetails['name'];
                }else{
                    $response['responseCode']="0";
                    $response['responseMessage']="Registration failed.";
                    $response['userId']=0;
                }
            }
            #Return response
            $response1=json_encode($response);
            echo $response1;
            
       }else{
        echo json_encode(array('responseCode'=>'0','responseMessage'=>'User type not define.'));          
    }



?>
