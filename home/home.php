<?php
/**
 * Created by PhpStorm.
 * User: ajay
 * Date: 4/28/2016
 * Time: 11:12 AM
 */

    class Home
    {
        
        public function getdriverslocations($data,$drivers)
        {
            $location = $data['location'];
            $nearest = array();
            foreach ($drivers as $driver)
            {
                $distance = self::getdistance($location['lat'],$location['long'],$driver->location['lat'],$driver->location['long']);

                $driver->distance = $distance['distance'];
                $driver->time = $distance['time'];
                $driver->distM = $distance['distM'];
                $nearest[] = $driver;
            }
            return $nearest;
        }

        public function getdistance($lat1, $lon1, $lat2, $lon2)
        { 
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$lon1."&destinations=".$lat2.",".$lon2."&mode=driving";

            $response = file_get_contents($url);

            $response_a = json_decode($response, true);
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $distM = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
           // return $dist .' , '.$time;
            return array('distance' => $dist, 'time' => $time,'distM'=>$distM);

        }

        public function getVehicleList()
        {
           $db = mysql_query("select * from vehicle_category");
            // var_dump($db);
            $vehicles= array();
            if(is_resource($db))
            {

                while($data = mysql_fetch_assoc($db))
                {
                    $vehicles[] = $data;
                }
                return $vehicles;
            }
            else
            {
                return $vehicles;
            }
        }

        public static function getHomeData($data,$drivers)
        {
          //  $driverData = self::getdriverslocations($data,$drivers);

            $vehicleList = self::getVehicleList();
          //  var_dump($vehicleList);die();
            $final = array();
            foreach ($vehicleList as $type)
            {
                foreach ($drivers as $driver)
                {
                    if($driver->vid == $type['id'])
                    { //echo $driver->vid;
                        $final['vehicle_details'][$type['id']][] = array('did'=>$driver->uid,'vehicle_type'=>$type['vehicle_type'],'vid'=>$type['id'],'d_lat'=>$driver->location['lat'],'d_long'=>$driver->location['long']);
                    }
                }
            }
            $final['u_location'] = $data['location'];
            $final['role'] = 'user';
            $final['action'] = 'home';
            $final['user_id'] = $data['user_id'];
           // var_dump($final);die();
            return $final;

        }


    }